import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Login from './components/Login';
import NotFound from './components/NotFound';
import AppLoading from './containers/AppLoading';
import NotificationsList from './containers/NotificationsList';
import AppRouter from './router/AppRouter';

const App = () => (
  <AppLoading>
    <Fragment>
      <BrowserRouter>
        <Switch>
          <Route path="/404" component={NotFound} />
          <Route exact={true} path="/login" component={Login} />
          <Route path="/" component={AppRouter} />
        </Switch>
      </BrowserRouter>
      <NotificationsList />
    </Fragment>
  </AppLoading>
);

export default App;
