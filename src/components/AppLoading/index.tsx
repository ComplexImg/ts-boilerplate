import React from 'react';
import s from './styles.module.scss';

interface Props {
  children: JSX.Element;
  loading: boolean;
}

class AppLoading extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    const { children, loading } = this.props;

    if (loading) {
      return <div className={s.appLoading}>Loading...</div>;
    }

    return children;
  }
}

export default AppLoading;
