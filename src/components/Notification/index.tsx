import cx from 'classnames';
import React from 'react';
import s from './styles.module.scss';

interface Props {
  children?: string;
  onCloseClick: () => void;
  title: string;
  variant: string; // oneOf(['success', 'warning', 'danger'])
}

const Notification = ({ children, onCloseClick, title, variant, ...rest }: Props) => (
  <div className={cx(s.notification, s[variant])} {...rest}>
    <button className={s.close} onClick={onCloseClick} type="button">
      Close
    </button>
    <div className={s.title}>{title}</div>
    {children}
  </div>
);

export default Notification;
