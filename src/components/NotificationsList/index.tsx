import { map } from 'ramda';
import React from 'react';
import { Notification as NotificationType } from '../../store/reducers/notifications';
import Notification from '../Notification';
import s from './styles.module.scss';

interface Props {
  notifications: NotificationType[];
  closeNotification: (id: string) => void;
}

const NotificationsList = ({ notifications, closeNotification }: Props) => (
  <div className={s.notifications}>
    {notifications.length > 0 &&
      map(({ id, title, description, variant }) => {
        const onClose = () => closeNotification(id);
        return (
          <Notification key={id} onCloseClick={onClose} title={title} variant={variant}>
            {description}
          </Notification>
        );
      }, notifications)}
  </div>
);

export default NotificationsList;
