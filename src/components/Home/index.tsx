import React from 'react';
import Hello from '../Hello';
import logo from './logo.svg';
import s from './styles.module.scss';

const Home = () => (
  <div className={s.app}>
    <header className={s.appHeader}>
      <img src={logo} className={s.appLogo} alt="logo" />
      <p>
        Edit <code>src/App.tsx</code> and save to reload.
      </p>
      <Hello name={'Slava'} />
    </header>
  </div>
);

export default Home;
