import React from 'react';
import { WrappedFieldProps } from 'redux-form';
import Input from '../Input';

const FormInput = ({ input, meta, ...rest }: WrappedFieldProps) => (
  <Input onChange={input.onChange} value={input.value} meta={meta} {...rest} />
);

export default FormInput;
