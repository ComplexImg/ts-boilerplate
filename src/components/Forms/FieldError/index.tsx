import cx from 'classnames';
import React from 'react';
import s from './styles.module.scss';

interface Props {
  children: string;
  className?: string;
}

const FieldError = ({ children, className, ...rest }: Props) => (
  <div className={cx(s.root, className)} {...rest}>
    {children}
  </div>
);

export default FieldError;
