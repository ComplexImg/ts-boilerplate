import { shallow } from 'enzyme';
import React from 'react';
import FieldError from './index';

test('CheckboxWithLabel changes the text after click', () => {
  const checkbox = shallow(<FieldError>children</FieldError>);

  expect(checkbox).toMatchSnapshot();
});
