import cx from 'classnames';
import React, { ComponentClass } from 'react';
import FieldError from '../FieldError';
import s from './styles.module.scss';

interface Meta {
  error?: string;
  touched?: boolean;
}

interface Props {
  children?: JSX.Element | string;
  className?: string;
  formComponent: ComponentClass<any>;
  label?: string;
  wrapperClassName?: string;
  errorClassName?: string;
  meta: Meta;
}

const FieldBase = ({
  children,
  errorClassName,
  className,
  formComponent: FormComponent,
  label,
  meta: { error, touched },
  wrapperClassName,
  ...rest
}: Props) => (
  <div className={cx(s.root, wrapperClassName)}>
    {label && <label>{label}</label>}
    <FormComponent error={!!(touched && error)} {...rest}>
      {children}
    </FormComponent>
    {!!(touched && error) && <FieldError className={errorClassName}>{error}</FieldError>}
  </div>
);

export default FieldBase;
