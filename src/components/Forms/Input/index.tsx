import cx from 'classnames';
import React from 'react';
import FieldBase from '../FieldBase';
import s from './styles.module.scss';

interface Props {
  className?: string;
  error?: boolean;
  type?: string;
}

const BasicInput = ({ className, error, type = 'text', ...rest }: Props) => (
  <input
    className={cx(s.root, className, {
      [s.error]: error,
    })}
    type={type}
    {...rest}
  />
);

const Input = (props: any) => <FieldBase formComponent={BasicInput} {...props} />;

export default Input;
