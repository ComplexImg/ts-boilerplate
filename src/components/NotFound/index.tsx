import React from 'react';
import s from './styles.module.scss';

const NotFound = () => <div className={s.pageNotFound}>404</div>;

export default NotFound;
