import i18n from '../../services/i18n';
import { User } from './index';

interface FormErrors {
  firstName?: string;
  age?: string;
}

function validate({ age, firstName }: User) {
  const errors: FormErrors = {};
  if (!firstName) {
    errors.firstName = i18n.t('validation:fieldRequired');
  } else if (firstName.length > 15) {
    errors.firstName = 'Must be 15 characters or less';
  }

  if (!age) {
    errors.age = i18n.t('validation:fieldRequired');
  } else if (isNaN(Number(age))) {
    errors.age = 'Must be a number';
  } else if (Number(age) < 18) {
    errors.age = 'Sorry, you must be at least 18 years old';
  }
  return errors;
}

export default validate;
