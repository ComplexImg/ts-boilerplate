import * as React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { Field, InjectedFormProps, reduxForm } from 'redux-form';
import FORMS from '../../constants/forms';

import FormInput from '../Forms/FormInput';
import validate from './validate';

interface Props {
  message: string;
}

export interface User {
  firstName?: string;
  age?: number;
}

class TestForm extends React.Component<Props & InjectedFormProps<User, Props> & WithNamespaces> {
  public render() {
    const { pristine, submitting, reset, handleSubmit, message, t } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <div>{message}</div>
        <div>
          <label>First Name </label>
          <Field name="firstName" component={FormInput} type="text" placeholder="First Name" />
        </div>
        <div>
          <label>Age</label>
          <Field name="age" component={FormInput} type="text" placeholder="Age" />
        </div>
        <div>
          <button type="submit" disabled={pristine || submitting}>
            {t('buttons.submit')}
          </button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>
            {t('buttons.clearValues')}
          </button>
        </div>
      </form>
    );
  }
}

export default reduxForm<User, Props>({
  form: FORMS.testForm,
  validate,
})(withNamespaces()(TestForm));
