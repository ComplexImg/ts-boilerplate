import React from 'react';
import { WithNamespaces, withNamespaces } from 'react-i18next';
import { Error } from 'tslint/lib/error';
import Button from '../../containers/Button';
import TestForm from '../../containers/TestForm';
import s from './styles.module.scss';

interface Props {
  name: string;
  level?: number;
}

const Hello = ({ name, level = 1, t }: Props & WithNamespaces) => {
  if (level <= 0) {
    throw new Error('You level is so low :(');
  }

  return (
    <div className={s.wrapper}>
      {t('hello')}, {name}. Your level's {level}
      <Button />
      <TestForm />
    </div>
  );
};

export default withNamespaces()(Hello);
