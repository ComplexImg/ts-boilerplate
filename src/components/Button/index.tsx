import React from 'react';
import s from './styles.module.scss';

interface Props {
  inc: () => void;
  dec: () => void;
  rand: () => void;
  notif: () => void;
  store: () => void;
  value: number;
}

class Button extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    const { inc, dec, notif, rand, store, value } = this.props;

    return (
      <div className={s.wrapper}>
        {`Result: ${value}`}
        <br />
        <button onClick={inc}>INC click me!</button>
        <br />
        <button onClick={dec}>DEC click me!</button>
        <br />
        <button onClick={rand}>RAND click me!</button>
        <br />
        <button onClick={notif}>Notification click me!</button>
        <br />
        <button onClick={store}>Storage click me!</button>
      </div>
    );
  }
}

export default Button;
