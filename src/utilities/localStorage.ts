import { assoc, forEach, forEachObjIndexed, reduce } from 'ramda';

export function getItem(key: string): string | null {
  return localStorage.getItem(key);
}

export function setItem(key: string, value: string): void {
  localStorage.setItem(key, value);
}

export function removeItem(key: string): void {
  localStorage.removeItem(key);
}

export function getItems(items: string[]): any {
  return reduce((acc: any, val: string) => assoc(val, localStorage.getItem(val), acc), {}, items);
}

export function setItems(items: any) {
  return forEachObjIndexed(
    (value: string, key: any) => value && localStorage.setItem(key, value),
    items
  );
}

export function removeItems(items: string[]) {
  return forEach((val: string) => localStorage.removeItem(val), items);
}
