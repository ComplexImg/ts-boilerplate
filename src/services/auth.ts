import { KEY_STORE, PUBLIC_KEY, TOKEN } from '../constants/localStorage';
import { getItems, removeItems, setItems } from '../utilities/localStorage';

const AUTH_DATA_KEYS = [TOKEN, KEY_STORE, PUBLIC_KEY];

function getAuthData() {
  const items = getItems(AUTH_DATA_KEYS);
  return Promise.resolve(items);
}

function setAuthData(data: any) {
  setItems(data);
  return Promise.resolve();
}

function clearAuthData() {
  removeItems(AUTH_DATA_KEYS);
  return Promise.resolve();
}

export { clearAuthData, setAuthData, getAuthData };
