import { getItem, removeItem, setItem } from '../utilities/localStorage';

function getStorageItem(key: string) {
  return () => {
    const items = getItem(key);
    return Promise.resolve(items);
  };
}

function setStorageItem(key: string, value: string) {
  return () => {
    setItem(key, value);
    return Promise.resolve();
  };
}

function removeStorageItem(key: string) {
  return () => {
    removeItem(key);
    return Promise.resolve();
  };
}

export { getStorageItem, setStorageItem, removeStorageItem };
