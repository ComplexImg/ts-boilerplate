import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import backend from 'i18next-xhr-backend';
import { reactI18nextModule } from 'react-i18next';

i18n
  .use(backend)
  .use(LanguageDetector)
  .use(reactI18nextModule)
  .init({
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    },
    debug: process.env.NODE_ENV === 'development',
    defaultNS: 'common',
    fallbackLng: {
      'en-US': ['en'],
      'ru-RU': ['ru'],
    },
    interpolation: {
      escapeValue: false,
    },
    load: 'languageOnly',
    ns: ['common', 'validation'],
    react: {
      bindI18n: 'languageChanged loaded',
      bindStore: 'added removed',
      wait: true,
    },
  });

export default i18n;
