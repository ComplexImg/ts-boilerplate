import { createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import App from './App';
import './index.scss';
import i18n from './services/i18n';
import * as serviceWorker from './serviceWorker';
import configureStore from './store/index';

const history = createBrowserHistory();

const initialState = window.initialReduxState;
const store = configureStore(history, initialState);

ReactDOM.render(
  <I18nextProvider i18n={i18n}>
    <Provider store={store}>
      <App />
    </Provider>
  </I18nextProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
