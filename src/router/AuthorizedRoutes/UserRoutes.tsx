import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Home from '../../components/Home';

const UserRoutes = () => (
  <Switch>
    <Route path="/" exact={true} component={Home} />
    <Redirect to="/404" />
  </Switch>
);

export default UserRoutes;
