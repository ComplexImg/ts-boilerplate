import React from 'react';
import { connect } from 'react-redux';

import { ADMIN, USER } from '../../constants/userRoles';
import { ApplicationState } from '../../store/reducers';
import { getUserRole } from '../../store/selectors/user';

import AdminRoutes from './AdminRoutes';
import UserRoutes from './UserRoutes';

interface Props {
  role: string;
}

const AuthorizedRoutes = ({ role }: Props) => {
  switch (role) {
    case USER:
      return <UserRoutes />;
    case ADMIN:
      return <AdminRoutes />;
    default:
      return null;
  }
};

const mapStateToProps = (state: ApplicationState) => ({
  role: getUserRole(state),
});

export default connect(mapStateToProps)(AuthorizedRoutes);
