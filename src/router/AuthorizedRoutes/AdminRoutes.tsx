import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import AdminPage from '../../components/AdminPage';

const AdminRoutes = () => (
  <Switch>
    <Route path="/" exact={true} component={AdminPage} />
    <Redirect to="/404" />
  </Switch>
);

export default AdminRoutes;
