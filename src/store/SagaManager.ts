import sagas from './sagas';

const SagaManager = {
  startSagas(sagaMiddleware: any) {
    sagas.forEach((saga: any) => sagaMiddleware.run(saga));
  },
};

export default SagaManager;
