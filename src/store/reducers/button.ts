import { Reducer } from 'redux';
import { ActionTypes } from '../actions/button';

export interface ButtonState {
  readonly value: number;
}

const initialState: ButtonState = {
  value: 0,
};

const buttonReducer: Reducer<ButtonState> = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.DEC: {
      return { value: state.value - 1 };
    }
    case ActionTypes.INC: {
      return { value: state.value + 1 };
    }
    default: {
      return state;
    }
  }
};

export { buttonReducer };
