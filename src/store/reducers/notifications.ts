import { prepend, propEq, reject } from 'ramda';
import { Reducer } from 'redux';
import { ActionTypes } from '../actions/notifications';

export interface Notification {
  id: string;
  title: string;
  time?: number;
  description: string;
  variant: string;
}

const initialState: Notification[] = [];

const notificationsReducer: Reducer<Notification[]> = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.ADD: {
      const { id, title, description, variant } = action.payload;
      return prepend({
        description,
        id,
        title,
        variant,
      })(state);
    }
    case ActionTypes.CLOSE: {
      const id = action.payload;
      return reject(propEq('id', id))(state);
    }
    case ActionTypes.CLEAR: {
      return [];
    }
    default: {
      return state;
    }
  }
};

export { notificationsReducer };
