import { combineReducers } from 'redux';
import { FormStateMap, reducer as formReducer } from 'redux-form';
import { buttonReducer, ButtonState } from './button';
import { Notification, notificationsReducer } from './notifications';

export interface ApplicationState {
  button: ButtonState;
  form: FormStateMap;
  notifications: Notification[];
}

export const rootReducer = combineReducers<ApplicationState>({
  button: buttonReducer,
  form: formReducer,
  notifications: notificationsReducer,
});
