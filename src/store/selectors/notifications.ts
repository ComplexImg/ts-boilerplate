import { prop } from 'ramda';

export const getNotifications = prop('notifications');
