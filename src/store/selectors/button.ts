import { prop } from 'ramda';
import { createSelector } from 'reselect';
import { ApplicationState } from '../reducers';
import { ButtonState } from '../reducers/button';

const localState = prop('button');

export const getButtonValue = createSelector<ApplicationState, ButtonState, number>(
  localState,
  prop('value')
);
