import { ApplicationState } from '../reducers';

export const getUserRole = (state: ApplicationState) => 'user';
