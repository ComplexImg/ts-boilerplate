import { action } from 'typesafe-actions';
import { Notification } from '../reducers/notifications';

export enum ActionTypes {
  ADD = 'NOTIFICATIONS/ADD',
  CLOSE = 'NOTIFICATIONS/CLOSE',
  CLEAR = 'NOTIFICATIONS/CLEAR',
}

export const notificationsAdd = (notification: Notification) =>
  action(ActionTypes.ADD, notification);

export const notificationsClose = (id: string) => action(ActionTypes.CLOSE, id);

export const notificationsClear = () => action(ActionTypes.CLEAR);
