import { action } from 'typesafe-actions';

export enum ActionTypes {
  DEC = 'BUTTON/DEC',
  INC = 'BUTTON/INC',
  NOTIF = 'BUTTON/NOTIF',
  RAND = 'BUTTON/RAND',
  STORAGE = 'BUTTON/STORAGE',
}

export const buttonInc = () => action(ActionTypes.INC);

export const buttonDec = () => action(ActionTypes.DEC);

export const buttonRand = () => action(ActionTypes.RAND);

export const buttonNotif = () => action(ActionTypes.NOTIF);

export const buttonStore = () => action(ActionTypes.STORAGE);
