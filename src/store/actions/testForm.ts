import { action } from 'typesafe-actions';

export enum ActionTypes {
  FORM_HANDLER = 'FORM/TEST_HANDLER',
}

export const testFormHandler = () => action(ActionTypes.FORM_HANDLER);
