// `react-router-redux` is deprecated, so we use `connected-react-router`.
// This provides a Redux middleware which connects to our `react-router` instance.
import { routerMiddleware } from 'connected-react-router';
// If you use react-router, don't forget to pass in your history type.
import { History } from 'history';
import { applyMiddleware, createStore, Store } from 'redux';

import { compose } from 'ramda';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import { ApplicationState, rootReducer } from './reducers';
import SagaManager from './SagaManager';

export default function configureStore(
  history: History,
  initialState: ApplicationState
): Store<ApplicationState> {
  // add redux-dev-tools
  const composeEnhancers =
    process.env.NODE_ENV === 'development' ? composeWithDevTools({}) : compose;

  const sagaMiddleware = createSagaMiddleware();

  // We'll create our store with the combined reducers/sagas, and the initial Redux state that
  // we'll be passing from our entry point.

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
  );

  SagaManager.startSagas(sagaMiddleware);

  return store;
}
