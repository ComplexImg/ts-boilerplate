import { call, put, select, take, takeEvery } from 'redux-saga/effects';
import i18n from '../../services/i18n';
import { getStorageItem, setStorageItem } from '../../services/localStorage';
import { getUniqId } from '../../utilities';
import { ActionTypes, buttonInc, buttonRand } from '../actions/button';
import { notificationsAdd } from '../actions/notifications';
import { Notification } from '../reducers/notifications';
import { getButtonValue } from '../selectors/button';

// tslint:disable:no-console
// todo need best action type !

function* onButtonRand(action: ReturnType<typeof buttonRand>) {
  console.log('hello');
  console.log('action', action);

  yield put(buttonInc());

  const value = yield select(getButtonValue);

  console.log('value now: ', value);

  yield take(ActionTypes.DEC);

  console.log('dec was clicked!');
}

function* onButtonNotif() {
  const notification: Notification = {
    description: i18n.t('notificationDescription'),
    id: getUniqId(),
    time: 1488,
    title: i18n.t('notificationTitle'),
    variant: 'danger',
  };

  yield put(notificationsAdd(notification));
}

function* onButtonStore() {
  yield call(setStorageItem('myItem', getUniqId()));

  const data = yield call(getStorageItem('myItem'));

  console.log('data', data);

  yield;
}

export default function*() {
  yield takeEvery(ActionTypes.RAND, onButtonRand);
  yield takeEvery(ActionTypes.NOTIF, onButtonNotif);
  yield takeEvery(ActionTypes.STORAGE, onButtonStore);
}
