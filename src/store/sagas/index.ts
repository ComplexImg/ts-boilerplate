import button from './button';
import notifications from './notifications';
import testForm from './testForm';

export default [button, notifications, testForm];
