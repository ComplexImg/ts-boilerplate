import { getFormValues, startSubmit, stopSubmit } from 'redux-form';
import { call, put, select, takeEvery } from 'redux-saga/effects';
import FORMS from '../../constants/forms';

import { ActionTypes } from '../actions/testForm';

// tslint:disable:no-console

function* onTestFormHandler() {
  yield put(startSubmit(FORMS.testForm));

  const data = yield select(getFormValues(FORMS.testForm));

  console.log('data form select', data);

  yield put(stopSubmit(FORMS.testForm));
}

export default function*() {
  yield takeEvery(ActionTypes.FORM_HANDLER, onTestFormHandler);
}
