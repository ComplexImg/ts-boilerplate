import { delay, put, takeEvery } from 'redux-saga/effects';
import { ActionTypes, notificationsAdd, notificationsClose } from '../actions/notifications';

function* onAddNotification(action: ReturnType<typeof notificationsAdd>) {
  const { id, time } = action.payload;

  if (time) {
    yield delay(time);
    yield put(notificationsClose(id));
  }
}

export default function* root() {
  yield takeEvery(ActionTypes.ADD, onAddNotification);
}
