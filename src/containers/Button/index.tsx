import { connect } from 'react-redux';

import Button from '../../components/Button';
import { getButtonValue } from '../../store/selectors/button';

import { Dispatch } from 'redux';
import {
  buttonDec,
  buttonInc,
  buttonNotif,
  buttonRand,
  buttonStore,
} from '../../store/actions/button';
import { ApplicationState } from '../../store/reducers';

const mapStateToProps = (state: ApplicationState) => ({
  value: getButtonValue(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  dec: () => dispatch(buttonDec()),
  inc: () => dispatch(buttonInc()),
  notif: () => dispatch(buttonNotif()),
  rand: () => dispatch(buttonRand()),
  store: () => dispatch(buttonStore()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Button);
