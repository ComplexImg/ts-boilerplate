import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import NotificationsList from '../../components/NotificationsList';
import { getNotifications } from '../../store/selectors/notifications';

import { notificationsClose } from '../../store/actions/notifications';
import { ApplicationState } from '../../store/reducers';

const mapStateToProps = (state: ApplicationState) => ({
  notifications: getNotifications(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  closeNotification: (id: string) => dispatch(notificationsClose(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationsList);
