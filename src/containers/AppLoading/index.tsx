import { connect } from 'react-redux';
import AppLoading from '../../components/AppLoading';
import { ApplicationState } from '../../store/reducers';

const mapStateToProps = (state: ApplicationState) => ({
  loading: false,
});

export default connect(mapStateToProps)(AppLoading);
