import { connect } from 'react-redux';

import TestForm from '../../components/TestForm';

import { Dispatch } from 'redux';

import { testFormHandler } from '../../store/actions/testForm';
import { ApplicationState } from '../../store/reducers';

const mapStateToProps = (state: ApplicationState) => ({
  message: 'msg!',
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: () => dispatch(testFormHandler()),
  // onSubmit: (data: any) => dispatch(testFormHandler(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestForm);
